const express =require('express');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const port = process.env.PORT || 8000;

app.get('/',(req,res) => {
    res.send('Welcome too API Restful: Express + TS + Swagger + Mongoose');

});

app.listen(port, () => {
    console.log(´EXPRESS SERVER: Running at http://localhost:${port}´)
})