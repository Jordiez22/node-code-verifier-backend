import {Express, Request, Response} from "express";
import dotenv from 'dotenv';

dotenv.config();

const app: EXPRESS = express();
const port: string | number = process.env.PORT || 8000;

app.get('/',(req: Request, res: Response) => {
    res.send('Welcome too API Restful: Express + TS + Swagger + Mongoose');

});

app.get('/hello', (req: Request, res: Response) =>{
    res.send('Welcome to GET Route: ¡Hello!');  
});
